# login_page

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# 项目说明

**由前端的几个组件组成的，只要有**

- 前端登录页面表单组件
- 数字时钟组件（简约）
- 3D地球组件（美观）

**使用技术/框架**

- vue
- vuetifyUI（https://vuetifyjs.com/zh-Hans/）
- echartsUI（https://echarts.apache.org/）

**效果：**

![img](./img/login.png)



![img](./img/earth.png)

# 总结

在开发中遇见一些挺好的组件，简单保留下来而已。如果能帮助到您，可以star一个，以后说不定能用上呢
