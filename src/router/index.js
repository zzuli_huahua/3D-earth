import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'hash',
    routes: [
        {
            path: '/',
            name: 'index',
            redirect: '/login',
            component: () => import('@/views/login/login')
        }, {
            path: '/login',
            name: 'Login',
            component: () => import('@/views/login/login')
        }, {
            path: '/home',
            name: 'Home',
            component: () => import('@/components/HelloWorld'),
            meta: '首页',
        },]
})

//实例化路由组件
// const routers = new Router({
//     mode: 'history',    // 直接访问 localhost:8080 即可，不用访问 localhost:8080/#
//     base: '',
//     router
// })
export default router
